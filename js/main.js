$(function() {
    $('.scroll').on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top }, 500, 'linear');
    });
});

// $('#owl-slider').owlCarousel({
//     loop: true,
//     margin: 10,
//     nav: false,
//     autoplay: true,
//     smartSpeed: 1500,
//     items: 1,
//     navText: ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>']
// })

$(document).on('click', '.dropdown-menu', function(e) {
    e.stopPropagation();
});

$('li.dropdown a.dropdown-toggle').on('click', function(event) {
    $(this).parent().toggleClass('open');
});


/*
        var $input = $( '.datepicker' ).pickadate({
            formatSubmit: 'yyyy/mm/dd',
            // min: [2015, 7, 14],
            container: '#container',
            // editable: true,
            closeOnSelect: true,
            closeOnClear: false,
        })

        var picker = $input.pickadate('picker')
        // picker.set('select', '14 October, 2014')
        // picker.open()

        // $('button').on('click', function() {
        //     picker.set('disable', true);
        // });
*/

jQuery(document).ready(function($) {
    $('#my-slider').sliderPro({
        width: 960,
        height: 500,
        arrows: true,
        buttons: false,
        waitForLayers: true,
        thumbnailWidth: 200,
        thumbnailHeight: 100,
        thumbnailPointer: true,
        autoplay: false,
        autoScaleLayers: false,
        breakpoints: {
            500: {
                thumbnailWidth: 120,
                thumbnailHeight: 50
            }
        }
    });
});


AOS.init({
    easing: 'ease-in-out-sine'
});
